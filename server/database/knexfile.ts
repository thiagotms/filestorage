export default {

  development: {
    client: "sqlite3",
    connection: {
      filename: "./database.sqlite3"
    }
  },

  staging: {
    client: "sqlite3",
    connection: {
      filename: "./database.sqlite3"
    }
  },

  production: {
    client: "sqlite3",
    connection: {
      filename: "./database.sqlite3"
    }
  },

};
