# TFileStorage
### Under Construction :exclamation:  

A little project to save a file in disk and information like name in database. Like 'dropbox' site.  
You can make download, update file name, upload a new file, get all files, get specific file information and delete a file.  

This project is not being done as a final product, so it should not be used in production.


## This project use :sparkles: 
:heavy_check_mark: Javascript(ES6+)    
:heavy_check_mark: TypeScript   
:heavy_check_mark: ExpressJS  
:heavy_check_mark: Jest(Unit testing)  
:heavy_check_mark: Swagger(Documentation)  => file-storage-swagger.json  
:heavy_check_mark: ObjectionJS  
:heavy_check_mark: Knex    
:heavy_check_mark: MVC  
:heavy_check_mark: CI(Github only)  


## Things to be made :clipboard:  

- [] Front-End(React). 

## Run Project
sudo npm install   
sudo npm run migrate  
sudo npm run dev    

## Run Test
sudo npm test  
